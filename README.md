# Window Manager and Desktop Environment

I use KDE with i3 as window manager. That works by setting the `KDEWM` environment variable.
The Pager Widget works with i3 and `i3-workspace-names-daemon` without complaint and also supports mouse interactions.
Only minor invonvenience is that is show the scratchpad windows on all workspaces.
I just really like the tiling of i3 and KDE brings things like a nice audio/bluetooth/network popup out of the box.
Instead of the color of the border I use a `compton` blur effect to mark inactive windows. 

## [i3-workspace-names-daemon](https://pypi.org/project/i3-workspace-names-daemon/)

In your config instead of specifying the name of the workspace you specify the number of the workspace.
The program then sets the name automatically based on what programs are open in the workspace.
You can use `.config/i3/app-icons.json` to specify pairs of program names and Font Awesome Icon names to assign icons to programs.

## Dropdown

I use the `scratchpad` workspace and the ability to apply filters to commands to create windows that are effectifely a dropdown:
I have a keybind to show them, briefly interact with them, and press the same key again to hide them.
I currently use this for my media player and a terminal.

## SO MANY WORKSPACES

The amount of workspaces isn't actually limited to 10 but when you only use the Meta key as a modifier you'll just run out of shortcuts after that.
However nothing stop you from assigning a second (or even third or fourth) modifier and using that to increase that number. I currently use Alt for workspace 11 to 20.
I rarely actually need the space, but you know it's nice to have the option.

Another application would be to use Alt+homerow keys for workspaces while keeping all other keybinds intact.

# Background
Inspired by [this reddit post](https://www.reddit.com/r/doctorwho/comments/2fby1g/edited_this_doctor_who_wallpaper_to_spotlight/).
[Images used](https://imgur.com/a/av5TP)

I use a selfmade script (`dwc.sh`) to set the Wallpaper based on the time of the day, and a systemd-timer to run that script every hour.
The script also regenerates the cached images for `betterlockscreen`.

# Lockscreen
I use [`betterlockscreen`](https://github.com/pavanjadhaw/betterlockscreen) with the dim option and the default config.
It has instructions for how to setup locking after suspend.

To automatically use it after some time I use `xautolock`. It will lock the screen after 15 minutes except when the mouse is in the top right corner (for when you're watching videos etc.)

# Steam
The steam window itself is a bit annoying when in tiling mode because it refuses to resize. To circumvent that you can start steam in floating mode or just put it on it's own workspace.
Games work out of the box in fullscreen mode. I use `STEAM_FRAME_FORCE_CLOSE=1 steam -silent` to start steam in the background/tray so that the Steam Controller works in the mouse config.

# Launcher (albert)
A Launcher is generally a way to launch your programs. Different DE/WMs have different ways of doing that e.g. windows startmenu. i3 uses dmenu, a very lightweight choice, by default: 
You map it to a shortcut, launch it by that shortcut and begin typing to search. You can select an entry with the arrow key and hit enter to select it. Once you get used to it, it is a very fast way to interact with your computer. An nice alternative with a few more bells and whistles is rofi. Both are very extensible and are actually more of a menu, that just happens to have a launcher menu built in.
That means you could use another shortcut to search for a file and open it. Or for selecting an action like shutdown/reboot etc. 
I personally use Albert, because it has all of that built in. It is far less customizable in terms of appearance, though so I might change back to rofi again in the future.

# Time Tracking (arbtt)

`arbtt` stands for automatic rule based time tracker, and is just that. It runs in the background, writes the active Xwindow to a file every minute or so and when you want to know where your last couple of hours went you just give it some rules to associate window titles with activities and let it tell you.

# File Synchronization (unison)
I use both a PC and a laptop, and it's just really nice when you can start something on your laptop and finish it on your PC when you come home. That means you need to synchronize your files somehow. Another benefit is you get a cheap if pretty bad means of backup. 

Syncthing is a nice option without a centralized server that does that automatically and that I used for some while, but we didn't really get along: It was probably my fault but on multiple occasions it are my files which is the opposite of what it should be doing.

Nowadays I use unison, which in it's core just compares to folders, tells you what's different between those and suggest which file to copy where to get and up to date version. It's not automatic and you need to provide the means of connection so that it can actually see these folders. But it tells me what it plans to do, so I can stop it from eating my files, ssh/scp is a thing and super versatile and the ignore-Syntax is easier to use. Plus it doesn't complain when I want to sync a folder with the laptop and then a subfolder of that with my phone and the option for recursive profiles makes that super easy.

# Password Manager (pass)
Password Managers are great. Seriously, use one!
As I said I want to sync stuff between laptop and PC and most Password managers make that hard, because they use a DB for storing your passwords, so the syncing program just tells you that both files have been edited in a conflict. pass works around that by storing every password in a simple plaintext file, and organizing them with normal folders. That way you only get a conflict if you have actually edited the same passwords in two different ways. To make sure only you have access to your passwords it encrypts them with gpg. The documentation is a bit sparse, but actually explains everything.
Some tricks I learned: 
- You can use different keys for different subdirectories
- You can use multiple keys for the same directory

If you don't want to open a terminal everytime you need a password (I usually just use the dropdown terminal) there are a lot of extensions for it on the official website and even some GUIs.

# Editor (Sublime with Vintage (vim-keybindings))
I learned to use vim when I first started using Linux and I really like the concept of modal editing. I used vim/neovim for a while but it always was a bit barebones on it's own and started to get sluggish when adding plugins to add functionality. Someone then introduces me to the great org-mode in emacs and, not wanting to learn another set of keybinds, I used spacemacs for a while.
Spacemacs is basically a set of Emacs configurations that make a lot of the keybinds easier to type and includes some cool emacs modes like e.g. `evil` that introduces vim-style keybinds. 

Then someone suggested Sublime to me. Sublime is proprietary, and technically only has a free trial (that is basically infinite though) but it is just so awesome. It has a lot of handy features and all of them just feel so snappy. And even though it's proprietary it's so easily extensible that you can find a plugin for quite a few things and through one of them it even supports vim-style modal editing while stile completely supporting the default style \o/.

So for just quickly editing a configuration, I still use vim, for anything more involved than that Sublime it is.

# Music (mpd + ncmpcpp and MusicBrainz Picard)
I don't really like where the distribution and sale of digital goods is going. If I pay for something I want to own it, in the sense that I can do whatever I like with it. I understand that for a lot of Content Producers DRM guarantees them income but that doesn't mean I have to like it. What's even worse is streaming services. Now I can't even access stuff offline. So I usually buy mp3s when I want some music or digitize CDs (which is legal as a private copy in my country, no idea about yours). MusicBrainz is a collaborative data base for information about music and Picard is an easy and pretty great way to tag those mp3s.

In terms of playing music mpd+ncmpcpp get me closest to what I want.

On Android I use this: https://github.com/MaxFour/Music-Player.

# KDE Connect
KDE Connect is a combo of an Android App and a Linux program that aims to integrate your phone with your PC. 

The shared clipboard feature is awesome enough to install in in my opinion, but it also shows your phone notifications on your PC, you can send files between the devices and you can use your phone to remote control your PC.
It doesn't do a lot, but what it does, it turns from a chore to really really easy and fast.

# TODO
- compton
- oh-my-zsh and .zshrc
- ledger
- File System
- Backup System
- Firefox Setup (mainly tree-style tabs)
