#!/bin/bash

image_path=/home/jana/Pictures/wallpaper/doctor_who_clock/dwc$(date +%I).jpg 
alt_path=/home/jana/Pictures/wallpaper/doctor_who_clock/dwc12.jpg

feh --bg-fill $image_path || feh --bg-fill $alt_path
betterlockscreen -u $image_path || betterlockscreen -u $alt_path
